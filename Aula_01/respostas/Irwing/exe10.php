


<?php 

$GrauCelsius = trim(fgets(STDIN));
$GrauFahrenheit = ($GrauCelsius * 1.8) + 32;

$format = sprintf("%01.2f", $GrauFahrenheit);
echo $format . "˚F";

?>