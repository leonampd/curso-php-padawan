<?php

do
{
    echo "Digite o valor da diagonal do quadrado";
    $d = trim(fgets(STDIN));
}
while (empty($d) || !is_numeric($d) || $d < 0);

$r = pow($d, 2) / 2;

echo "$r é o valor da área do quadrado.";

?>
