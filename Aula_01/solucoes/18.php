<?php
// Entrar via teclado com o valor de cinco produtos. Após as entradas, digitar um valor referente ao
// pagamento da somatória destes valores. Calcular e exibir o troco que deverá ser devolvido.


$p1 = trim(fgets(STDIN));
$p2 = trim(fgets(STDIN));
$p3 = trim(fgets(STDIN));
$p4 = trim(fgets(STDIN));
$p5 = trim(fgets(STDIN));

$soma = $p1 + $p2 +  $p3 + $p4 + $p5;

echo "Sua compra somou: R$ $soma. Digite o valor do pagamento: \n";

$pagamento = trim(fgets(STDIN));

$troco = $pagamento - $soma;

echo $troco."\n";