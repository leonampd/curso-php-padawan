<?php

// Entrar via teclado com a base e a altura de um retângulo, calcular e exibir sua área

$base   = trim(fgets(STDIN));
$altura = trim(fgets(STDIN));

$area = $base * $altura;

echo $area;
echo "\n";