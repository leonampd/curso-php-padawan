<?php
    date_default_timezone_set('America/Sao_Paulo');

    require "classIdade.php";

    echo "Digite a sua data de nascimento no formato dd/mm/aaaa";

    $dia = trim(fgets(STDIN));
    $mes = trim(fgets(STDIN));
    $ano = trim(fgets(STDIN));

    $idade = new Idade($dia, $mes, $ano);

    do
    {
        $valida = false;

        try
        {
            $valida = $idade->validar_dados($dia, $mes, $ano);
        }
        catch(InvalidArgumentException $e)
        {
            echo "Digite a sua data de nascimento no formato dd/mm/aaaa";

            $dia = trim(fgets(STDIN));
            $mes = trim(fgets(STDIN));
            $ano = trim(fgets(STDIN));
        }
    }while($valida == false);

    $anos = $idade->calcular_idade($dia, $mes, $ano);

    echo "$anos";

?>
