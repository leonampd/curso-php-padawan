<?php

class Materia
{
    private $titulo;
    private $slug;
    private $corpo;
    private $tags;
    private $imagem_destaque;

    /**
     * Gets the value of titulo.
     *
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Sets the value of titulo.
     *
     * @param mixed $titulo the titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Gets the value of slug.
     *
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Sets the value of slug.
     *
     * @param mixed $slug the slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Gets the value of corpo.
     *
     * @return mixed
     */
    public function getCorpo()
    {
        return $this->corpo;
    }

    /**
     * Sets the value of corpo.
     *
     * @param mixed $corpo the corpo
     *
     * @return self
     */
    public function setCorpo($corpo)
    {
        $this->corpo = $corpo;

        return $this;
    }

    /**
     * Gets the value of tags.
     *
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Sets the value of tags.
     *
     * @param mixed $tags the tags
     *
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Gets the value of imagem_destaque.
     *
     * @return mixed
     */
    public function getImagemDestaque()
    {
        return $this->imagemDestaque;
    }

    /**
     * Sets the value of imagem_destaque.
     *
     * @param mixed $imagem_destaque the imagem_destaque
     *
     * @return self
     */
    public function setImagemDestaque($imagemDestaque)
    {
        $this->imagemDestaque = $imagemDestaque;

        return $this;
    }
}


	
?>
