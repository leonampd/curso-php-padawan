<?php

class ImportarMateria
{
    private $urlApi;
    private $marca;


    /**
     * Gets the value of urlApi.
     *
     * @return mixed
     */

    public function getUrlApi()
    {
        return $this->concatenar();
        /* Já concatenar aqui em vez de chamar no script */
    }

    /**
     * Sets the value of urlApi.
     *
     * @param mixed $urlApi the url api
     *
     * @return self
     */

    public function setUrlApi($urlApi)
    {
        $this->urlApi = $urlApi;

        return $this;
    }

    /**
     * Gets the value of marca.
     *
     * @return mixed
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Sets the value of marca.
     *
     * @param mixed $marca the marca
     *
     * @return self
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    public function concatenar(){
    	$urlConcatenada = $this->urlApi."?marca=".$this->marca;

    	return $urlConcatenada;
    }

    public function obterDados(){

        $this->r = file_get_contents($this->getUrlApi());

        return $this->r;
    }

    public function jsonToArray() {

        return json_decode($this->r);
    }
}



?>
