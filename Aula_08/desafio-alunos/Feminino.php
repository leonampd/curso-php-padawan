<?php
require_once 'Pessoa.php';
require_once 'Calculo.php';

    class Feminino extends Pessoa implements Calculo{
        public function __construct($nome, $peso, $altura) {
            $sexo = 'Feminino';
            parent::__construct($nome, $peso, $sexo, $altura);
        }

        public function Calcular(){
            $r = $this->peso / ($this->altura * $this->altura);
            if($r < 19){
                return "Abaixo do peso";
            }
            else if($r >= 19 && $r < 24){
                return "Peso Ideal";
            }
            else {
                return "Acima do peso";
            }
        }
    }
?>